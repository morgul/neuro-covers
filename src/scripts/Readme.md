# Scripts Folder

These are standalone scripts that can be run from the command line. They are not part of the main application.

## Scripts

* `import_tags.ts` - Imports tags from the `/config/overrides.yml` file into the database.
