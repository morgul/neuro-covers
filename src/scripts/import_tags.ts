import { parse as parseYML } from 'yaml';
import { resolve } from 'node:path';

// Interfaces
import { TrackImage, TagDetails } from '../interfaces/tags.js';

// Resource Access
import filesRA from '../resource-access/files.js';
import { GoogleDriveResourceAccess } from '../resource-access/googleDrive.js';
import tagsRA from '../resource-access/tags.js';

// ---------------------------------------------------------------------------------------------------------------------

const tagOverrides = parseYML(await filesRA.readFileAsString(resolve('./config/overrides.yml')));

// ---------------------------------------------------------------------------------------------------------------------

async function main() : Promise<void>
{
    console.log('Starting import...');

    const driveRA = new GoogleDriveResourceAccess();
    for(const folderName in tagOverrides)
    {
        console.log(`Processing folder: ${ folderName }`);

        // Create or update db tags from tagOverrides
        for(const fileName in tagOverrides[folderName])
        {
            console.log(`Processing file: ${ fileName }`);

            const tagOverride = tagOverrides[folderName][fileName];
            const fileItem = await driveRA.getByFileName(fileName);
            if(!fileItem)
            {
                console.error(`Google Drive Item not found: ${ fileName }`);

                // eslint-disable-next-line no-continue
                continue;
            }

            // Set the cover data correctly
            let coverAlbum : TagDetails['coverAlbum'] = null;
            let coverArtist : TagDetails['coverArtist'] = [];
            let image : TrackImage | undefined = undefined;
            switch (folderName)
            {
                case 'anniversary':
                    coverAlbum = '1st Anniversary';
                    coverArtist = [ 'Neuro-sama', 'Evil Neuro' ];
                    image = {
                        mime: 'image/jpeg',
                        url: 'https://wiki.chriscase.io/images/neuro/neuro_anniversary_1.jpeg'
                    };
                    break;
                case 'duets':
                    coverAlbum = 'Duets';
                    coverArtist = [ 'Neuro-sama', 'Evil Neuro' ];
                    image = {
                        mime: 'image/jpeg',
                        url: 'https://wiki.chriscase.io/images/neuro/neuro_v3.jpeg'
                    };
                    break;
                case 'evil':
                    coverAlbum = 'Evil v1';
                    coverArtist = 'Evil Neuro',
                    image = {
                        mime: 'image/png',
                        url: 'https://wiki.chriscase.io/images/neuro/evil_nero_v1.png'
                    };
                    break;
                case 'v2':
                    coverAlbum = 'v2';
                    coverArtist = 'Neuro-sama';
                    image = {
                        mime: 'image/jpeg',
                        url: 'https://wiki.chriscase.io/images/neuro/neuro_v2.jpeg'
                    };
                    break;
                case 'v1':
                    coverAlbum = 'v1';
                    coverArtist = 'Neuro-sama';
                    image = {
                        mime: 'image/jpeg',
                        url: 'https://wiki.chriscase.io/images/neuro/neuro_v1.jpeg'
                    };
                    break;
                default:
                    coverAlbum = 'v3';
                    coverArtist = 'Neuro-sama';
                    image = {
                        mime: 'image/jpeg',
                        url: 'https://wiki.chriscase.io/images/neuro/neuro_v3.jpeg'
                    };
                    break;
            }

            // TagDetails
            const tag = {
                fileID: fileItem.id,
                title: tagOverride.title,
                artist: tagOverride.artist,
                album: tagOverride.album ?? null,
                coverArtist: tagOverride.coverArtist ?? coverArtist,
                coverAlbum: tagOverride.coverAlbum ?? coverAlbum,
                image,
                track: tagOverride.track ?? null,
                year: tagOverride.year ?? null
            };

            const existingTag = await tagsRA.get(fileItem.id);
            if(existingTag)
            {
                await tagsRA.update(fileItem.id, tag);
            }
            else
            {
                await tagsRA.create(tag);
            }
        }
    }
}

// ---------------------------------------------------------------------------------------------------------------------

main()
    .then(() =>
    {
        console.log('Done');
        process.exit(0);
    })
    .catch((err) =>
    {
        console.error(err);
        process.exit(1);
    });

// ---------------------------------------------------------------------------------------------------------------------
