// ---------------------------------------------------------------------------------------------------------------------
// Tag Interfaces
// ---------------------------------------------------------------------------------------------------------------------

export interface NeuroTags
{
    artist : string | string[];
    album : string;
    image : {
        mime : string;
        url : string;
    }
}

export interface TrackInfo
{
    artist : string | string[];
    title : string;
    album ?: string | null;
    year ?: number | null;
    releaseDate ?: string | undefined;
}

export interface TrackImage
{
    mime : string;
    url : string;
}

export interface TagDetails extends Omit<TrackInfo, 'releaseDate'>
{
    fileID : string | null;
    coverArtist : string | string[];
    coverAlbum ?: string | null;
    track ?: number | null;
    image ?: TrackImage | undefined;
}

export interface TrackGuess
{
    artist : string | undefined;
    title : string | undefined;
}

export type TagOverrides = Record<string, TrackInfo>;

export interface TagConfig
{
    neuro : NeuroTags;
    overrides ?: TagOverrides;
}

// ---------------------------------------------------------------------------------------------------------------------
