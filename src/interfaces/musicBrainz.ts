// ---------------------------------------------------------------------------------------------------------------------
// MusicBrainz API Interfaces
// ---------------------------------------------------------------------------------------------------------------------

export interface MusicBrainzArtist
{
    id : string;
    name : string;
    'sort-name' : string;
    disambiguation : string;
}

export interface MusicBrainzReleaseEvent
{
    date : string;
    area : {
        id : string;
        name : string;
        'sort-name' : string;
    };
}

export interface MusicBrainzCoverArt
{
    id : string;
    types : string[];
    front : boolean;
    back : boolean;
    edit : number;
    image : string;
}

export interface MusicBrainzRelease
{
    id : string;
    title : string;
    date : string;
    country : string;
    releaseevents : MusicBrainzReleaseEvent[];
    'cover-art-archive' : MusicBrainzCoverArt;
}

export interface MusicBrainzMetadata
{
    id : string;
    title : string;
    length : number;
    disambiguation : string;
    video : boolean;
    'artist-credit' : MusicBrainzArtist[];
    'first-release-date' : string;
    releases ?: MusicBrainzRelease[];
}

// ---------------------------------------------------------------------------------------------------------------------
