//----------------------------------------------------------------------------------------------------------------------
// Google Drive Interfaces
//----------------------------------------------------------------------------------------------------------------------

export interface GDriveItem
{
    id : string;
    name : string;
    mimeType : string;
    md5Checksum : string;
    webViewLink : string;
    modifiedTime : string;
    createdTime : string;
    parents : string[];
}

export interface GDriveFile extends GDriveItem
{
    size : string;
}

export interface GDriveFolder extends GDriveItem
{
    mimeType : 'application/vnd.google-apps.folder';
}

export interface GDriveFolderContents
{
    files : GDriveFile[];
    folders : GDriveFolder[];
}

//----------------------------------------------------------------------------------------------------------------------
