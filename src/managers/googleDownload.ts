//----------------------------------------------------------------------------------------------------------------------
// Google Download Manager
//----------------------------------------------------------------------------------------------------------------------

import { homedir } from 'node:os';
import { resolve, join } from 'node:path';
import * as cliProgress from 'cli-progress';

// Interfaces
import { GDriveFile, GDriveFolder, GDriveFolderContents } from '../interfaces/googleDrive.js';

// Engines
import { GoogleDriveSyncEngine } from '../engines/googleDrive.js';

// Resource Access
import filesRA from '../resource-access/files.js';
import { GoogleDriveResourceAccess } from '../resource-access/googleDrive.js';

//----------------------------------------------------------------------------------------------------------------------

const driveRA = new GoogleDriveResourceAccess();
const driveEng = new GoogleDriveSyncEngine();

//----------------------------------------------------------------------------------------------------------------------

export class GoogleDownloadManager
{
    async $download(files : GDriveFile[], rootFolder : string, progressBar ?: cliProgress.GenericBar) : Promise<string[]>
    {
        const updatedFiles : string[] = [];
        for(const file of files)
        {
            // Sanitize file name
            file.name = file.name.replaceAll('/', '-');

            // Build file path
            const filePath = join(rootFolder, file.name);

            // Update the progress bar
            progressBar?.update({ filename: file.name });

            // Download the file
            const data = await driveRA.downloadFile(file.id);
            await filesRA.writeFile(filePath, data);

            // Update the list of updated files
            updatedFiles.push(file.name);

            // Increment progress bar
            progressBar?.increment();
        }

        return updatedFiles;
    }

    async $downloadFolder(
        folders : GDriveFolder[],
        files : GDriveFile[],
        rootFolder : string,
        progressBar ?: cliProgress.GenericBar, recursive = false
    ) : Promise<string[]>
    {
        let updatedFiles : string[] = [];

        for(const folder of folders)
        {
            // Sanitize folder name
            const folderName = folder.name.replaceAll('/', '-');

            // Update root path
            const folderPath = join(rootFolder, folderName);

            // Filter files to just those in the folder
            const subFiles = files.filter((file) => file.parents.includes(folder.id));

            // Download files
            const dlFiles = await this.$download(subFiles, folderPath, progressBar);

            // Download folders if recursive
            if(recursive)
            {
                const contents = driveEng.splitContents(files);
                const filteredFolders = contents.folders.filter((fldr) => fldr.parents.includes(folder.id));
                const folderFiles = contents.files.filter((file) => file.parents.includes(folder.id));
                updatedFiles = [
                    ...updatedFiles,
                    ...await this.$downloadFolder(filteredFolders, folderFiles, rootFolder, progressBar, recursive)
                ];
            }

            // Update the list of updated files
            updatedFiles = [ ...updatedFiles, ...dlFiles ];
        }

        return updatedFiles;
    }

    async getFolderContents(folderID : string, recursive = false) : Promise<GDriveFolderContents>
    {
        const items = await driveRA.listFolder(folderID, recursive);
        return driveEng.splitContents(items);
    }

    async download(rootFolder : string, folderID : string, recursive = false) : Promise<string[]>
    {
        let updatedFiles : string[] = [];
        rootFolder = rootFolder.replace('~', homedir());

        // Create root folder
        await filesRA.mkdir(resolve(rootFolder));

        // Create progress bar
        const progressBar = new cliProgress.SingleBar(
            {
                clearOnComplete: true,
                format: 'Downloading: {value}/{total} [{bar}] {percentage}% | ETA: {eta_formatted} | {filename}'
            },
            cliProgress.Presets.shades_classic
        );

        // Files to download
        const files = (await driveEng.getFilesToDownload(folderID, rootFolder, recursive))
            .map((file) =>
            {
                file.name = file.name.replaceAll('/', '-');
                return file;
            });
        const contents = driveEng.splitContents(files);

        // // Get total file count
        const totalFiles = driveEng.countFilesInFolder(files, folderID, recursive);

        // Start the progress bar
        progressBar.start(totalFiles, 0);

        // Download files
        const folderContents = contents.files.filter((file) => file.parents.includes(folderID));
        updatedFiles = await this.$download(folderContents, rootFolder, progressBar);

        // Download folders if recursive
        if(recursive)
        {
            const folders = contents.folders.filter((folder) => folder.parents.includes(folderID));
            const folderFiles = contents.files.filter((file) => file.parents.includes(folderID));
            updatedFiles = [ ...updatedFiles, ...await this.$downloadFolder(folders, folderFiles, rootFolder, progressBar) ];
        }

        // Sync the database
        await driveEng.syncToDB(files);

        // Stop the progress bar
        progressBar.stop();

        // Return the updated files
        return updatedFiles;
    }
}

//----------------------------------------------------------------------------------------------------------------------
