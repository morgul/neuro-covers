// ---------------------------------------------------------------------------------------------------------------------
// Tagging Manager
// ---------------------------------------------------------------------------------------------------------------------

import { join } from 'node:path';
import { readFile } from 'node:fs/promises';
import * as mm from 'music-metadata';

// Interfaces
import { MusicBrainzMetadata } from '../interfaces/musicBrainz.js';
import { TagConfig, TrackGuess, TrackInfo } from '../interfaces/tags.js';

// Resource Access
import { AcoustIDResourceAccess } from '../resource-access/acoustid.js';
import { MusicBrainzResourceAccess } from '../resource-access/musicbrainz.js';
import { ID3TagResourceAccess } from '../resource-access/id3Tags.js';
import { GoogleDriveResourceAccess } from '../resource-access/googleDrive.js';
import tagRA from '../resource-access/tags.js';

// ---------------------------------------------------------------------------------------------------------------------

const acoustIDAPI = JSON.parse(await readFile('./keys/acoustid.json', 'utf-8')) ?? { key: '' };

const acoustidRA = new AcoustIDResourceAccess(acoustIDAPI.key);
const googleDriveRA = new GoogleDriveResourceAccess();
const musicBrainzRA = new MusicBrainzResourceAccess();
const id3TagRA = new ID3TagResourceAccess();

// ---------------------------------------------------------------------------------------------------------------------

export class TaggingManager
{
    async _getExistingTags(file : string) : Promise<TrackInfo>
    {
        const existingMetadata = await mm.parseFile(file);

        console.log('Existing Metadata:', existingMetadata.common);

        const title = existingMetadata.common.title ?? '';
        const album = existingMetadata.common.album ?? '';
        const year = existingMetadata.common.year ?? -1;
        const releaseDate = existingMetadata.common.date ?? '';

        const originalArtist = existingMetadata.common.originalartist;
        const artists = existingMetadata.common.artists;
        const artist = originalArtist ?? ((artists?.length ?? 0) > 0 ? artists : existingMetadata.common.artist) ?? '';

        return {
            title,
            artist,
            album,
            year,
            releaseDate
        };
    }

    async _getMusicBrainzTags(file : string) : Promise<TrackInfo>
    {
        const acoustIDResults = await acoustidRA.getAcoustIDMatches(file);
        const tagResults = await Promise.all(acoustIDResults.results.map(async (result) =>
        {
            const recordings : MusicBrainzMetadata[] = [];

            if(result.recordings && result.recordings.length > 0)
            {
                // Get track info from MusicBrainz for all recordings
                for(const recording of result.recordings)
                {
                    const trackInfo = await musicBrainzRA.getRecordingInfo(recording.id);
                    if(trackInfo)
                    {
                        recordings.push(trackInfo);

                        // Only grab the first recording that works
                        break;
                    }
                }
            }

            return recordings;
        }));

        // Flatten the array of arrays
        let tags = tagResults.flat();

        // Filter out any empty results
        tags = tags.filter((tag) => tag !== undefined);

        // Filter out any videos
        tags = tags.filter((tag) => !tag.video);

        // Grab the most likely title and artist
        const title = tags[0]?.title ?? '';
        let artist = tags[0]?.['artist-credit'].map((item) => item.name) ?? '';
        const album = tags[0]?.releases?.[0]?.title ?? '';
        const year = tags[0]?.['first-release-date']?.split('-')[0] ?? '-1';
        const releaseDate = tags[0]?.['first-release-date'] ?? '';

        // If artist is an array with a single string, convert it to a string
        if(Array.isArray(artist) && artist.length === 1)
        {
            artist = artist[0] ?? 'Unknown';
        }

        return {
            title,
            artist,
            album,
            year: parseInt(year),
            releaseDate
        };
    }

    _getTagsFromFilename(filename : string) : TrackGuess
    {
        let [ title, artist ] = filename.split(' - ');

        if(title)
        {
            // Only sentence case the title if it's not already capitalized
            if(title === title.toLowerCase())
            {
                title = title.split(' ').map((word) => word.charAt(0).toUpperCase() + word.slice(1))
                    .join(' ');
            }
        }

        if(artist)
        {
            if(artist === artist.toLowerCase())
            {
                // Sentence case the artist
                artist = artist.split(' ').map((word) => word.charAt(0).toUpperCase() + word.slice(1))
                    .join(' ');
            }

            // Remove date from artist(if exists)
            artist = artist.replace(/ \(\d+\s\d+\s\d+\)/, '').replace('.mp3', '');

            artist = artist.split(' (')[0];
        }

        return {
            title,
            artist
        };
    }

    combineTags(existingTags : TrackInfo, musicBrainzTags : TrackInfo, filenameTags : TrackGuess) : TrackInfo
    {
        return {
            title: existingTags.title || musicBrainzTags.title || filenameTags.title || '',
            artist: existingTags.artist || musicBrainzTags.artist || filenameTags.artist || '',
            album: (existingTags.album || musicBrainzTags.album) ?? null,
            year: (existingTags.year === -1 ? musicBrainzTags.year : existingTags.year) ?? null,
            releaseDate: existingTags.releaseDate || musicBrainzTags.releaseDate
        };
    }

    async tagFolder(path : string, filesToTag : string[], tagConfig : TagConfig) : Promise<void>
    {
        for(const file of filesToTag)
        {
            let tags : TrackInfo = {
                artist: '',
                title: '',
                album: '',
                year: 1900,
                releaseDate: ''
            };

            const dbTags = await tagRA.getByName(file);
            if(dbTags)
            {
                console.log(`Tagging from DB: '${ file }'`);
                if(Array.isArray(dbTags.artist))
                {
                    dbTags.artist = dbTags.artist.join(' & ');
                }

                const tagDetails = {
                    title: dbTags.title,
                    artist: dbTags.artist,
                    album: dbTags.coverAlbum ?? tagConfig.neuro.album,
                    year: dbTags.year ?? 1900,
                    originalArtist: dbTags.artist,
                    releaseDate: undefined,
                    image: dbTags.image ?? tagConfig.neuro.image
                };

                await id3TagRA.writeId3Tags(join(path, file), tagDetails);
            }
            else
            {
                console.log(`Tagging: '${ file }'`);
                const musicBrainzTags = await this._getMusicBrainzTags(join(path, file));
                const existingTags = await this._getExistingTags(join(path, file));
                const filenameTags = this._getTagsFromFilename(file);
                tags = this.combineTags(existingTags, musicBrainzTags, filenameTags);

                const originalArtist = Array.isArray(tags.artist) ? tags.artist.join(' & ') : tags.artist;
                const artist = Array.isArray(tagConfig.neuro.artist)
                    ? tagConfig.neuro.artist.join(' & ') : tagConfig.neuro.artist;

                if(!tags.title || !tags.artist)
                {
                    console.log(`No tags found, skipping: '${ file }'`);
                    // eslint-disable-next-line no-continue
                    continue;
                }

                const tagDetails = {
                    title: tags.title,
                    artist,
                    album: tagConfig.neuro.album,
                    year: tags.year ?? 1900,
                    originalArtist,
                    releaseDate: tags.releaseDate,
                    image: tagConfig.neuro.image
                };

                const driveItem = await googleDriveRA.getByFileName(file);
                if(driveItem)
                {
                    await tagRA.create({
                        fileID: driveItem.id,
                        title: tags.title,
                        artist: originalArtist,
                        album: tags.album ?? null,
                        coverAlbum: tagConfig.neuro.album,
                        coverArtist: tagConfig.neuro.artist,
                        year: tags.year ?? null,
                        image: tagConfig.neuro.image
                    });
                }

                await id3TagRA.writeId3Tags(join(path, file), tagDetails);
            }
        }
    }
}

// ---------------------------------------------------------------------------------------------------------------------
