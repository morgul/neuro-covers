//----------------------------------------------------------------------------------------------------------------------
// Neuro Cover Downloader / Tagger
//----------------------------------------------------------------------------------------------------------------------

import { homedir } from 'node:os';
import { join } from 'node:path';
import { program } from 'commander';

// Managers
import { GoogleDownloadManager } from './managers/googleDownload.js';
import { TaggingManager } from './managers/tagging.js';

// Resource Access
import filesRA from './resource-access/files.js';

//----------------------------------------------------------------------------------------------------------------------

const googleDlMan = new GoogleDownloadManager();
const taggingMan = new TaggingManager();

//----------------------------------------------------------------------------------------------------------------------

program
    .option('-r --root <root_folder>', 'Root folder to download to', '~/Music/Neuro')
    .option('-f --folder <folder_id>', 'Folder ID to download from', '118gr4QuaGQGKfJ0X8VBCytvPjdzPayPY')
    .option('--skip-sync', 'Skip syncing from Google Drive', false)
    .option('--anniversary', 'Tag the Anniversary folder', false)
    .option('--duets', 'Tag the Duets folder', false)
    .option('--evil', 'Tag the Evil folder', false)
    .option('--v1', 'Tag the v1 voice folder', false)
    .option('--v2', 'Tag the v2 voice folder', false)
    .option('--tag-all', 'Tag all folders', false)
    .parse();

//----------------------------------------------------------------------------------------------------------------------

const { root, folder, skipSync, tagAll } = program.opts();
let { anniversary, duets, evil, v1, v2 } = program.opts();
if(tagAll)
{
    anniversary = true;
    duets = true;
    evil = true;
    v1 = true;
    v2 = true;
}

if(!root)
{
    process.exit(1);
}

//----------------------------------------------------------------------------------------------------------------------
// Main Script
//----------------------------------------------------------------------------------------------------------------------

console.log('Neuro Cover Downloader / Tagger\n');

const folderID : string = folder;
const rootFolder : string = root.replace('~', homedir());

//----------------------------------------------------------------------------------------------------------------------
// Sync from Google Drive
//----------------------------------------------------------------------------------------------------------------------

let filesToTag : string[] = [];
if(!skipSync)
{
    console.log('Syncing from Google Drive...');

    if(tagAll)
    {
        // Sync all folders
        console.log('Syncing all folders...');
        await googleDlMan.download(rootFolder, folderID, true);
    }
    else
    {
        console.log('Syncing specific folders...');

        // Get folder contents
        console.log('Getting root folder contents...');
        const neuroAudioShare = await googleDlMan.getFolderContents(folderID);

        // Sync root folder (non-recursive)
        console.log('Syncing root folder...');
        filesToTag = await googleDlMan.download(rootFolder, folderID);

        // -------------------------------------------------------------------------------------------------------------
        // Sync specific folders
        // -------------------------------------------------------------------------------------------------------------

        const anniversaryFolderID = neuroAudioShare.folders.find((fldr) => fldr.name === 'Anniversary')?.id;
        if(anniversary && anniversaryFolderID)
        {
            console.log('Syncing anniversary folder...');
            const files = await googleDlMan.download(join(rootFolder, 'Anniversary'), anniversaryFolderID);
            filesToTag = filesToTag.concat(files.map((file) => join('Anniversary', file)));
        }

        const duetsFolderID = neuroAudioShare.folders.find((fldr) => fldr.name === 'Duets')?.id;
        if(duets && duetsFolderID)
        {
            console.log('Syncing duets folder...');
            const files = await googleDlMan.download(join(rootFolder, 'Duets'), duetsFolderID);
            filesToTag = filesToTag.concat(files.map((file) => join('Duets', file)));
        }

        const evilFolderID = neuroAudioShare.folders.find((fldr) => fldr.name === 'Evil')?.id;
        if(evil && evilFolderID)
        {
            console.log('Syncing evil folder...');
            const files = await googleDlMan.download(join(rootFolder, 'Evil'), evilFolderID);
            filesToTag = filesToTag.concat(files.map((file) => join('Evil', file)));
        }

        const v1VoiceFolderID = neuroAudioShare.folders.find((fldr) => fldr.name === 'v1 voice')?.id;
        if(v1 && v1VoiceFolderID)
        {
            console.log('Syncing v1 voice folder...');
            const files = await googleDlMan.download(join(rootFolder, 'v1 voice'), v1VoiceFolderID);
            filesToTag = filesToTag.concat(files.map((file) => join('v1 voice', file)));
        }

        const v2VoiceFolderID = neuroAudioShare.folders.find((fldr) => fldr.name === 'v2 voice')?.id;
        if(v2 && v2VoiceFolderID)
        {
            console.log('Syncing v2 voice folder...');
            const files = await googleDlMan.download(join(rootFolder, 'v2 voice'), v2VoiceFolderID);
            filesToTag = filesToTag.concat(files.map((file) => join('v2 voice', file)));
        }
    }

    console.log('Sync Complete!\n');
}

//----------------------------------------------------------------------------------------------------------------------
// Manage Files to Tag
//----------------------------------------------------------------------------------------------------------------------

if(tagAll)
{
    filesToTag = (await filesRA.listDir(rootFolder, true))
        .map((file) => file.replace(rootFolder, '').replace(/^\//, ''));
}
else if(skipSync)
{
    if(anniversary)
    {
        filesToTag = filesToTag.concat(await filesRA.listDir(join(rootFolder, 'Anniversary')));
    }

    if(duets)
    {
        filesToTag = filesToTag.concat(await filesRA.listDir(join(rootFolder, 'Duets')));
    }

    if(evil)
    {
        filesToTag = filesToTag.concat(await filesRA.listDir(join(rootFolder, 'Evil')));
    }

    if(v1)
    {
        filesToTag = filesToTag.concat(await filesRA.listDir(join(rootFolder, 'v1 voice')));
    }

    if(v2)
    {
        filesToTag = filesToTag.concat(await filesRA.listDir(join(rootFolder, 'v2 voice')));
    }

    filesToTag = filesToTag
        .filter((file) => file.endsWith('.mp3'))
        .map((file) => file.replace(rootFolder, '').replace(/^\//, ''));
}

// Note: We only support tagging mp3 files
filesToTag = filesToTag.filter((file) => file.endsWith('.mp3'));

//----------------------------------------------------------------------------------------------------------------------
// Tag Specific Folders
//----------------------------------------------------------------------------------------------------------------------

console.log('Tagging files...');
if(filesToTag.length > 0)
{
    console.log(`Tagging root folder...`);
    await taggingMan.tagFolder(
        rootFolder,
        filesToTag.filter((file) => !file.includes('/')),
        {
            neuro: {
                artist: 'Neuro-sama',
                album: 'v3',
                image: {
                    mime: 'image/jpeg',
                    url: 'https://wiki.chriscase.io/images/neuro/neuro_v3.jpeg'
                }
            }
        }
    );

    if(anniversary)
    {
        console.log(`Tagging 'Anniversary' folder...`);
        await taggingMan.tagFolder(
            join(rootFolder, 'Anniversary'),
            filesToTag.filter((file) => file.startsWith('Anniversary/')).map((file) => file.replace('Anniversary/', '')),
            {
                neuro: {
                    artist: [ 'Neuro-sama', 'Evil Neuro' ],
                    album: '1st Anniversary',
                    image: {
                        mime: 'image/jpeg',
                        url: 'https://wiki.chriscase.io/images/neuro/neuro_anniversary_1.jpeg'
                    }
                }
            }
        );
    }

    if(duets)
    {
        console.log(`Tagging 'Duets' folder...`);
        await taggingMan.tagFolder(
            join(rootFolder, 'Duets'),
            filesToTag.filter((file) => file.startsWith('Duets/')).map((file) => file.replace('Duets/', '')),
            {
                neuro: {
                    artist: [ 'Neuro-sama', 'Evil Neuro' ],
                    album: 'Duets',
                    image: {
                        mime: 'image/jpeg',
                        url: 'https://wiki.chriscase.io/images/neuro/neuro_v3.jpeg'
                    }
                }
            }
        );
    }

    if(evil)
    {
        console.log(`Tagging 'Evil' folder...`);
        await taggingMan.tagFolder(
            join(rootFolder, 'Evil'),
            filesToTag.filter((file) => file.startsWith('Evil/')).map((file) => file.replace('Evil/', '')),
            {
                neuro: {
                    artist: 'Evil Neuro',
                    album: 'Evil v1',
                    image: {
                        mime: 'image/png',
                        url: 'https://wiki.chriscase.io/images/neuro/evil_nero_v1.png'
                    }
                }
            }
        );
    }

    if(v1)
    {
        console.log(`Tagging 'v1 voice' folder...`);
        await taggingMan.tagFolder(
            join(rootFolder, 'v1 voice'),
            filesToTag.filter((file) => file.startsWith('v1 voice/')).map((file) => file.replace('v1 voice/', '')),
            {
                neuro: {
                    artist: 'Neuro-sama',
                    album: 'v1',
                    image: {
                        mime: 'image/jpeg',
                        url: 'https://wiki.chriscase.io/images/neuro/neuro_v1.jpeg'
                    }
                }
            }
        );
    }

    if(v2)
    {
        console.log(`Tagging 'v2 voice' folder...`);
        await taggingMan.tagFolder(
            join(rootFolder, 'v2 voice'),
            filesToTag.filter((file) => file.startsWith('v2 voice/')).map((file) => file.replace('v2 voice/', '')),
            {
                neuro: {
                    artist: 'Neuro-sama',
                    album: 'v2',
                    image: {
                        mime: 'image/jpeg',
                        url: 'https://wiki.chriscase.io/images/neuro/neuro_v2.jpeg'
                    }
                }
            }
        );
    }
}
else
{
    console.log('...No files to tag!');
}

//----------------------------------------------------------------------------------------------------------------------

console.log('Tagging Complete!');
process.exit(0);

//----------------------------------------------------------------------------------------------------------------------
