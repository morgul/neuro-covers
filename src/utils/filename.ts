// ---------------------------------------------------------------------------------------------------------------------
// filename.ts
// ---------------------------------------------------------------------------------------------------------------------

export function sanitizeFilename(filename : string) : string
{
    // Spit filename between name and extension
    const fileParts = filename.split('.');
    const fileExtension = fileParts.pop();
    const fileNameNoExt = fileParts.join('.');
    const cleanedFileName = fileNameNoExt.replaceAll('/', '-').trim();

    return cleanedFileName + (fileExtension ? `.${ fileExtension }` : '');
}

// ---------------------------------------------------------------------------------------------------------------------
