// ---------------------------------------------------------------------------------------------------------------------
// Database Utility
// ---------------------------------------------------------------------------------------------------------------------

import { resolve } from 'node:path';
import { readFile } from 'node:fs/promises';
import knex, { Knex } from 'knex';
import { parse as parseYML } from 'yaml';

// Interfaces
import { DatabaseConfig, ExtendedKnexConfig } from '../interfaces/config.js';

// ---------------------------------------------------------------------------------------------------------------------

type AfterCreateCallback = (conn : any, done : any) => void;

// ---------------------------------------------------------------------------------------------------------------------

let dbInst : Knex | undefined;

// ---------------------------------------------------------------------------------------------------------------------

function _buildCustomAfterCreate(config : ExtendedKnexConfig, afterCreate : AfterCreateCallback) : ExtendedKnexConfig
{
    // Modify the config to enable query tracing or foreign key constraints
    const _afterCreate = config?.pool?.afterCreate ?? ((_conn, done) => done());

    // Create a new 'afterCreate' function that sets up sqlite.
    const newAfterCreate = (dbConn, done) : void =>
    {
        afterCreate(dbConn, (err) =>
        {
            if(err)
            {
                done(err);
            }
            else
            {
                _afterCreate?.(dbConn, done);
            }
        });
    };

    return {
        ...config,
        pool: {
            ...config.pool,
            afterCreate: newAfterCreate
        }
    };
}

function _buildSqliteDB(config : ExtendedKnexConfig) : Knex
{
    const newConf = _buildCustomAfterCreate(config, (dbConn, done) =>
    {
        if(config.traceQueries)
        {
            // Turn on tracing
            dbConn.on('trace', (queryString) =>
            {
                console.log('QUERY:', queryString);
            });
        }

        // Turn on foreign key constraints and WAL mode
        dbConn.exec('PRAGMA foreign_keys = ON', (err) =>
        {
            if(err)
            {
                return done(err);
            }

            dbConn.exec('PRAGMA journal_mode = WAL', (err2) =>
            {
                done(err2, dbConn);
            });
        });

        done();
    });

    return knex(newConf);
}

function _buildDB(config : Knex.Config) : Knex
{
    return knex(config);
}

// ---------------------------------------------------------------------------------------------------------------------

export function buildDB(config : ExtendedKnexConfig) : Knex
{
    switch (config.client)
    {
        case 'sqlite3':
        case 'better-sqlite3':
            return _buildSqliteDB(config);
        default:
            return _buildDB(config);
    }
}

export async function getDBConfig() : Promise<ExtendedKnexConfig>
{
    const configYAML = await readFile(resolve(import.meta.dirname, '..', '..', 'config/db.yml'), 'utf8');
    const config = parseYML(configYAML) as DatabaseConfig;

    return config.database;
}

export async function getDB() : Promise<Knex>
{
    if(dbInst)
    {
        return dbInst;
    }
    else
    {
        const dbConfig = await getDBConfig();
        return buildDB(dbConfig);
    }
}

// ---------------------------------------------------------------------------------------------------------------------
