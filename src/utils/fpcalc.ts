// ---------------------------------------------------------------------------------------------------------------------
// fpcalc Wrapper
// ---------------------------------------------------------------------------------------------------------------------

import fpcalc from 'fpcalc';

// ---------------------------------------------------------------------------------------------------------------------

interface FpCalcResult
{
    duration : number;
    file : string;
    fingerprint : string | Buffer;
}

interface FpCalcOptions
{
    length ?: number;
    raw ?: boolean;
    command ?: string;
}

// ---------------------------------------------------------------------------------------------------------------------

export async function calcAudioFP(path : string, options : FpCalcOptions = {}) : Promise<FpCalcResult>
{
    return new Promise((resolve, reject) =>
    {
        fpcalc(path, options, (error, result) =>
        {
            if(error)
            {
                reject(error);
            }
            else
            {
                resolve(result as FpCalcResult);
            }
        });
    });
}

// ---------------------------------------------------------------------------------------------------------------------
