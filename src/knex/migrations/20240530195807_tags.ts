//----------------------------------------------------------------------------------------------------------------------

import type { Knex } from 'knex';

//----------------------------------------------------------------------------------------------------------------------

export async function up(knex : Knex) : Promise<void>
{
    await knex.schema.createTable('tag', (table) =>
    {
        table.increments('id');
        table.string('file_id')
            .references('gdrive_item.id')
            .notNullable()
            .onUpdate('CASCADE')
            .onDelete('CASCADE');
        table.string('title').notNullable();
        table.json('artists').notNullable();
        table.json('cover_artists').notNullable();
        table.string('album').nullable();
        table.string('cover_album').nullable();
        table.string('cover_art').nullable();
        table.string('cover_art_mime').nullable();
        table.integer('track').nullable();
        table.integer('year').nullable();
    });
}

//----------------------------------------------------------------------------------------------------------------------

export async function down(knex : Knex) : Promise<void>
{
    await knex.schema.dropTable('tag');
}

//----------------------------------------------------------------------------------------------------------------------
