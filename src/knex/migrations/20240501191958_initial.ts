//----------------------------------------------------------------------------------------------------------------------

import type { Knex } from 'knex';

//----------------------------------------------------------------------------------------------------------------------

export async function up(knex : Knex) : Promise<void>
{
    await knex.schema.createTable('gdrive_item', (table) =>
    {
        table.string('id').primary();
        table.string('name').notNullable();
        table.string('mime_type').notNullable();
        table.string('md5_checksum').nullable();
        table.string('web_view_link').notNullable();
        table.timestamp('modified').notNullable();
        table.timestamp('created').notNullable();
    });

    await knex.schema.createTable('gdrive_parent', (table) =>
    {
        table.increments('id');
        table.string('file_id')
            .references('gdrive_item.id')
            .notNullable()
            .onUpdate('CASCADE')
            .onDelete('CASCADE');
        table.string('parent_id')
            .notNullable();

        table.unique([ 'file_id', 'parent_id' ]);
    });
}

//----------------------------------------------------------------------------------------------------------------------

export async function down(knex : Knex) : Promise<void>
{
    await knex.schema.dropTable('gdrive_item');
    await knex.schema.dropTable('fs_item');
}

//----------------------------------------------------------------------------------------------------------------------
