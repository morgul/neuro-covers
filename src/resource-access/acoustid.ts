//----------------------------------------------------------------------------------------------------------------------
// AcoustIDResourceAccess
//----------------------------------------------------------------------------------------------------------------------

import axios from 'axios';

// Utils
import { calcAudioFP } from '../utils/fpcalc.js';

//----------------------------------------------------------------------------------------------------------------------

interface AcoustIDRecording
{
    id : string;
    title ?: string;
    duration ?: number;
    artists ?: {
        id : string;
        name : string;
    }[];
}

interface AcoustIDResult
{
    id : string;
    score : number;
    recordings ?: AcoustIDRecording[];
}

interface AcoustIDLookupResponse
{
    status : string;
    results : AcoustIDResult[];
}

//----------------------------------------------------------------------------------------------------------------------

export class AcoustIDResourceAccess
{
    #apiKey : string;

    constructor(apiKey : string)
    {
        if(!apiKey)
        {
            throw new Error('An API key is required to use AcoustID.');
        }

        this.#apiKey = apiKey;
    }

    async _getAcoustIDFromFP(duration : number, fingerprint : string | Buffer) : Promise<AcoustIDLookupResponse | null>
    {
        try
        {
            const response = await axios.get('https://api.acoustid.org/v2/lookup', {
                params: {
                    client: this.#apiKey,
                    format: 'json',
                    duration,
                    fingerprint,
                    meta: 'recordingids'
                }
            });

            return response.data;
        }
        catch (error)
        {
            if(axios.isAxiosError(error))
            {
                if(error.response)
                {
                    if(error.response.status >= 400 && error.response.status < 500)
                    {
                        console.error('Client-side error:', error.response.status, error.response.statusText);
                        if(error.response.data)
                        {
                            console.error('Response Body:', error.response.data);
                        }
                    }
                    else if(error.response.status >= 500)
                    {
                        console.error('Server-side error: The service is down.');
                    }
                }
                else if(error.request)
                {
                    console.error('No response was received', error.request);
                }
                else
                {
                    console.error('Error', error.message);
                }
            }
            else
            {
                console.error('Error', error);
            }
        }

        return null;
    }

    //------------------------------------------------------------------------------------------------------------------

    async getAcoustIDMatches(audioFile : string) : Promise<AcoustIDLookupResponse>
    {
        const fpResult = await calcAudioFP(audioFile);
        const meta = await this._getAcoustIDFromFP(fpResult.duration, fpResult.fingerprint);

        if(meta && meta.results)
        {
            meta.results.sort((item1, item2) => item2.score - item1.score);
        }

        return meta ?? { status: 'not-found', results: [] };
    }
}

//----------------------------------------------------------------------------------------------------------------------
