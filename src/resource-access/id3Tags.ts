//----------------------------------------------------------------------------------------------------------------------
// ID3TagResourceAccess
//----------------------------------------------------------------------------------------------------------------------

import axios from 'axios';
// import { Promise as nodeID3, Tags as NodeID3Tags } from 'node-id3';

import NodeID3 from 'node-id3';

const nodeID3 = NodeID3.Promise;
type NodeID3Tags = NodeID3.Tags;

//----------------------------------------------------------------------------------------------------------------------

export interface TagDetails
{
    title : string;
    artist : string;
    album : string;
    year : number;
    originalArtist : string;
    releaseDate ?: string | undefined;
    image ?: {
        mime : string;
        url : string;
    }
}

//----------------------------------------------------------------------------------------------------------------------

export class ID3TagResourceAccess
{
    imageCache : Map<string, Buffer> = new Map();

    /**
     * Writes IDv3 tags to a mp3, based on the TagDetails object provided.
     *
     * @param filePath - The path of the mp3 to update.
     * @param details - The tags to apply to the mp3 file.
     */
    async writeId3Tags(filePath : string, details : TagDetails) : Promise<void>
    {
        const commentText = `Cover of '${ details.originalArtist } - ${ details.title }' by ${ details.artist }.`;

        const tags : NodeID3Tags = {
            title: details.title,
            artist: details.artist,
            album: details.album,
            originalArtist: details.originalArtist,
            performerInfo: details.artist,
            remixArtist: details.artist,
            year: `${ details.year }`
        };

        if(details.releaseDate)
        {
            tags.date = details.releaseDate;
        }

        if(details.image)
        {
            let imageBuf = this.imageCache.get(details.image.url);
            if(!imageBuf)
            {
                const { data } = await axios.get(details.image.url, { responseType: 'arraybuffer' });
                if(data)
                {
                    imageBuf = Buffer.from(data, 'binary');
                    this.imageCache.set(details.image.url, imageBuf);
                }
            }

            // We should have set this, but if we still failed, handle it.
            if(imageBuf)
            {
                tags.image = {
                    mime: details.image.mime,
                    type: {
                        id: 3,
                        name: 'front cover'
                    },
                    description: 'album art',
                    imageBuffer: imageBuf
                };
            }
        }

        tags.comment = {
            language: 'eng',
            text: commentText
        };

        await nodeID3.write(tags, filePath);
    }
}

//----------------------------------------------------------------------------------------------------------------------
