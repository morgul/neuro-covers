//----------------------------------------------------------------------------------------------------------------------
// TagResourceAccess
//----------------------------------------------------------------------------------------------------------------------

// Interfaces
import { TagDetails, TrackImage, TrackInfo } from '../interfaces/tags.js';

// Utils
import { getDB } from '../utils/database.js';

//----------------------------------------------------------------------------------------------------------------------

class TagResourceAccess
{
    async list() : Promise<TrackInfo[]>
    {
        const db = await getDB();
        return db('tag').select('*');
    }

    async get(fileID : string) : Promise<TagDetails | null>
    {
        const db = await getDB();
        const [ tag ] = await db('tag').select('*')
            .where({ file_id: fileID });

        if(!tag)
        {
            return null;
        }

        let image : TrackImage | undefined = undefined;
        if(tag.cover_art)
        {
            image = {
                url: tag.cover_art,
                mime: tag.cover_art_mime
            };
        }

        return {
            fileID: tag.file_id,
            title: tag.title,
            album: tag.album,
            coverAlbum: tag.cover_artist,
            artist: JSON.parse(tag.artist ?? '[]'),
            coverArtist: JSON.parse(tag.cover_artist ?? '[]'),
            image,
            track: tag.track,
            year: tag.year
        };
    }

    async getByName(name : string) : Promise<TagDetails | null>
    {
        const db = await getDB();
        const [ file ] = await db('gdrive_item')
            .select('id')
            .where({ name });

        if(!file)
        {
            return null;
        }

        return this.get(file.id);
    }

    async create(tag : TagDetails) : Promise<TagDetails>
    {
        if(!tag.fileID)
        {
            throw new Error('File ID is required');
        }

        const db = await getDB();
        await db('tag').insert({
            file_id: tag.fileID,
            title: tag.title,
            album: tag.album,
            cover_album: tag.coverAlbum,
            artists: JSON.stringify(tag.artist ?? []),
            cover_artists: JSON.stringify(tag.coverArtist ?? []),
            track: tag.track ?? 1,
            cover_art: tag.image?.url ?? null,
            cover_art_mime: tag.image?.mime ?? null,
            year: tag.year
        });

        const newTag = await this.get(tag.fileID);
        if(!newTag)
        {
            throw new Error('Failed to create tag');
        }

        return newTag;
    }

    async update(fileID : string, tag : TagDetails) : Promise<TagDetails>
    {
        const db = await getDB();
        await db('tag').update({
            file_id: tag.fileID,
            title: tag.title,
            album: tag.album,
            cover_album: tag.coverAlbum,
            artists: JSON.stringify(tag.artist ?? []),
            cover_artists: JSON.stringify(tag.coverArtist ?? []),
            cover_art: tag.image?.url ?? null,
            cover_art_mime: tag.image?.mime ?? null,
            track: tag.track ?? 1,
            year: tag.year
        })
            .where({ file_id: fileID });

        const newTag = await this.get(fileID);
        if(!newTag)
        {
            throw new Error('Failed to create tag');
        }

        return newTag;
    }

    async delete(tagID : string) : Promise<void>
    {
        const db = await getDB();
        await db('tag').delete()
            .where({ tag_id: tagID });
    }
}

//----------------------------------------------------------------------------------------------------------------------

export default new TagResourceAccess();

//----------------------------------------------------------------------------------------------------------------------
