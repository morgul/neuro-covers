//----------------------------------------------------------------------------------------------------------------------
// Google Drive Resource Access
//----------------------------------------------------------------------------------------------------------------------

import { resolve } from 'node:path';
import { google } from 'googleapis';

// Interfaces
import { GDriveFolderContents, GDriveItem, GDriveFile, GDriveFolder } from '../interfaces/googleDrive.js';

// Utils
import { getDB } from '../utils/database.js';
import { getAuthClient } from '../utils/googleAuth.js';
import { sanitizeFilename } from '../utils/filename.js';

//----------------------------------------------------------------------------------------------------------------------

export class GoogleDriveResourceAccess
{
    #googleAuth : any;

    constructor(serviceAccountKeyFile = './keys/service-account.json')
    {
        const keyFilePath = resolve(serviceAccountKeyFile);
        this.#googleAuth = getAuthClient(keyFilePath, [ 'https://www.googleapis.com/auth/drive' ]);
    }

    async listFolder(folderId : string, recursive = false) : Promise<GDriveItem[]>
    {
        const drive = google.drive({ version: 'v3', auth: this.#googleAuth });
        let pageToken : string | undefined = undefined;
        let items : GDriveItem[] = [];

        do
        {
            const results = await drive.files.list({
                q: `'${ folderId }' in parents`,
                fields: 'nextPageToken, files(id, name, md5Checksum, mimeType, webViewLink, modifiedTime, createdTime, size, parents)',
                pageToken: pageToken as string
            });

            items = items.concat(results.data.files);
            pageToken = results.data.nextPageToken;
        }
        while(pageToken);

        // Recursively list sub folders
        if(recursive)
        {
            for(const item of items)
            {
                // Replace the name
                item.name = sanitizeFilename(item.name);

                if(item.mimeType === 'application/vnd.google-apps.folder')
                {
                    items = items.concat(await this.listFolder(item.id, recursive));
                }
            }
        }

        return items;
    }

    async splitFolderContents(contents : GDriveItem[]) : Promise<GDriveFolderContents>
    {
        return {
            files: contents.filter((item) => item.mimeType !== 'application/vnd.google-apps.folder') as GDriveFile[],
            folders: contents.filter((item) => item.mimeType === 'application/vnd.google-apps.folder') as GDriveFolder[]
        };
    }

    async downloadFile(fileId : string) : Promise<Buffer>
    {
        const drive = google.drive({ version: 'v3', auth: this.#googleAuth });
        const { data } = await drive.files.get({
            fileId,
            alt: 'media'
        }, { responseType: 'arraybuffer' });

        return Buffer.from(data as ArrayBuffer);
    }

    // -----------------------------------------------------------------------------------------------------------------
    // Database Operations
    // -----------------------------------------------------------------------------------------------------------------

    async addDBItem(file : GDriveItem) : Promise<void>
    {
        const db = await getDB();
        await db.transaction(async(trx) =>
        {
            await trx('gdrive_item')
                .insert({
                    id: file.id,
                    name: file.name,
                    mime_type: file.mimeType,
                    web_view_link: file.webViewLink,
                    md5_checksum: file.md5Checksum,
                    modified: file.modifiedTime,
                    created: file.createdTime
                })
                .onConflict([ 'id' ])
                .merge();

            const fileParents = file.parents.map((parentID) => ({
                file_id: file.id,
                parent_id: parentID
            }));

            await trx('gdrive_parent')
                .insert(fileParents)
                .onConflict([ 'file_id', 'parent_id' ])
                .ignore();
        });
    }

    async getByFileName(fileName : string) : Promise<GDriveItem | null>
    {
        const db = await getDB();
        const item = await db('gdrive_item')
            .select()
            .where('name', fileName)
            .first();

        if(!item)
        {
            return null;
        }

        return {
            id: item.id,
            name: item.name,
            mimeType: item.mime_type,
            md5Checksum: item.md5_checksum,
            webViewLink: item.web_view_link,
            modifiedTime: item.modified,
            createdTime: item.created,
            parents: [ item.parent_id ]
        };
    }

    async listFolderDB(folderID : string, recursive = false) : Promise<GDriveItem[]>
    {
        const db = await getDB();
        let items = await db('gdrive_item as gdi')
            .select('gdi.*', 'gdp.parent_id')
            .join('gdrive_parent as gdp', 'gdi.id', 'gdp.file_id')
            .where('gdp.parent_id', folderID);

        if(recursive)
        {
            for(const item of items)
            {
                if(item.mimeType === 'application/vnd.google-apps.folder')
                {
                    items = items.concat(await this.listFolderDB(item.id, recursive));
                }
            }
        }

        return items.reduce((acc, item) =>
        {
            const existingItem = acc.find((accItem) => accItem.id === item.id);
            if(existingItem)
            {
                existingItem.parents.push(item.parent_id);
            }
            else
            {
                acc.push({
                    id: item.id,
                    name: item.name,
                    mimeType: item.mime_type,
                    md5Checksum: item.md5_checksum,
                    webViewLink: item.web_view_link,
                    modifiedTime: item.modified,
                    createdTime: item.created,
                    parents: [ item.parent_id ]
                });
            }

            return acc;
        }, []);
    }
}

//----------------------------------------------------------------------------------------------------------------------
