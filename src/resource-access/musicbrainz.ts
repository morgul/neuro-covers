//----------------------------------------------------------------------------------------------------------------------
// MusicBrainzResourceAccess
//----------------------------------------------------------------------------------------------------------------------

import axios from 'axios';
import rateLimit from 'axios-rate-limit';

// Interfaces
import { MusicBrainzMetadata } from '../interfaces/musicBrainz.js';

//----------------------------------------------------------------------------------------------------------------------

export class MusicBrainzResourceAccess
{
    private http : any;

    constructor()
    {
        // @ts-expect-error Fuck you, TypeScript
        this.http = rateLimit(axios.create({
            headers: {
                'User-Agent': 'NeuroCovers/0.5.0 ( chris.case@g33xnexus.com )'
            }
        }), { maxRequests: 1, perMilliseconds: 1200 });
    }

    async getRecordingInfo(recordingId : string) : Promise<MusicBrainzMetadata | null>
    {
        try
        {
            const response = await this.http.get(`https://musicbrainz.org/ws/2/recording/${ recordingId }?inc=artists+releases&fmt=json`);
            return response.data;
        }
        catch (error)
        {
            if(axios.isAxiosError(error))
            {
                if(error.response)
                {
                    if(error.response.status >= 400 && error.response.status < 500)
                    {
                        console.error('Client-side error:', error.response.status, error.response.statusText);
                        if(error.response.data)
                        {
                            console.error('Response Body:', error.response.data);
                        }
                    }
                    else if(error.response.status >= 500)
                    {
                        console.error('Server-side error: The service is down.');
                    }
                }
                else if(error.request)
                {
                    console.error('No response was received', error.request);
                }
                else
                {
                    console.error('Error', error.message);
                }
            }
            else
            {
                console.error('Error', error);
            }
        }

        return null;
    }
}

//----------------------------------------------------------------------------------------------------------------------
