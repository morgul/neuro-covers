// ---------------------------------------------------------------------------------------------------------------------
// Files Resource Access
// ---------------------------------------------------------------------------------------------------------------------

import { access, constants, readdir, readFile, mkdir, stat, writeFile } from 'node:fs/promises';
import { resolve } from 'node:path';

// ---------------------------------------------------------------------------------------------------------------------

class FileResourceAccess
{
    async exists(filePath : string) : Promise<boolean>
    {
        try
        {
            await access(filePath, constants.F_OK);
            return true;
        }
        catch
        {
            return false;
        }
    }

    async isFileUnchanged(filePath : string, compareDate : Date, size : number) : Promise<boolean>
    {
        const stats = await stat(filePath);
        return stats.mtime >= compareDate && stats.size === size;
    }

    async mkdir(dirPath : string) : Promise<void>
    {
        await mkdir(dirPath, { recursive: true });
    }

    async listDir(dirPath : string, recursive = true) : Promise<string[]>
    {
        // List the contents of the directory, possibly recursively
        const files : string[] = [];
        const dirents = await readdir(dirPath, { withFileTypes: true });

        for(const dirent of dirents)
        {
            const fullPath = resolve(dirPath, dirent.name);

            if(dirent.isDirectory())
            {
                if(recursive)
                {
                    files.push(...await this.listDir(fullPath));
                }
            }
            else
            {
                files.push(fullPath);
            }
        }

        return files;
    }

    async readFileAsString(filePath : string) : Promise<string>
    {
        // Resolve to a full path
        filePath = resolve(filePath);

        if(!await this.exists(filePath))
        {
            throw new Error(`File '${ filePath }' does not exist!`);
        }

        return readFile(filePath, { encoding: 'utf8' });
    }

    async writeFile(filePath : string, contents : string | Buffer) : Promise<void>
    {
        // Resolve to a full path
        filePath = resolve(filePath);

        await writeFile(filePath, contents);
    }
}

// ---------------------------------------------------------------------------------------------------------------------

export default new FileResourceAccess();

// ---------------------------------------------------------------------------------------------------------------------
