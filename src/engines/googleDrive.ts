// ---------------------------------------------------------------------------------------------------------------------
// Google Drive Sync Engine
// ---------------------------------------------------------------------------------------------------------------------

// Interfaces
import { GDriveFile, GDriveFolder, GDriveFolderContents, GDriveItem } from '../interfaces/googleDrive.js';

// Resource Access
import filesRA from '../resource-access/files.js';
import { GoogleDriveResourceAccess } from '../resource-access/googleDrive.js';

//----------------------------------------------------------------------------------------------------------------------

const driveRA = new GoogleDriveResourceAccess();

// ---------------------------------------------------------------------------------------------------------------------

export class GoogleDriveSyncEngine
{
    /**
     * Split a list of GDriveItems into files and folders.
     *
     * @param contents - List of GDriveItems, should be the contents of a folder.
     *
     * @returns An object containing the files and folders.
     */
    splitContents(contents : GDriveItem[]) : GDriveFolderContents
    {
        return {
            files: contents.filter((item) => item.mimeType !== 'application/vnd.google-apps.folder') as GDriveFile[],
            folders: contents.filter((item) => item.mimeType === 'application/vnd.google-apps.folder') as GDriveFolder[]
        };
    }

    /**
     * Given a list of GDriveItems, count the number of files in a folder.
     *
     * @param contents - List of GDriveItems, should be the contents of the folder.
     * @param folderID - ID of the folder to count files in.
     * @param recursive - Whether to count files in subfolders.
     *
     * @returns The number of files in the folder.
     */
    countFilesInFolder(contents : GDriveItem[], folderID : string, recursive = false) : number
    {
        let fileCount = 0;
        const childFiles = contents.filter((item) => item.parents.includes(folderID));

        if(recursive)
        {
            const childFolders = childFiles.filter((item) => item.mimeType === 'application/vnd.google-apps.folder');
            for(const folder of childFolders)
            {
                fileCount += this.countFilesInFolder(contents, folder.id, recursive);
            }
        }

        return fileCount + childFiles.filter((item) => item.mimeType !== 'application/vnd.google-apps.folder').length;
    }

    /**
     * Given a list of GDriveItems, check if any files are missing from a folder on the filesystem, and return them.
     *
     * @param items - List of GDriveItems, should be limited to just the contents of the folder.
     * @param folderPath - Path to the folder on the filesystem.
     * @param recursive - Whether to check files in subfolders.
     *
     * @returns A list of GDriveItems that are missing from the folder.
     */
    async findMissingFiles(items : GDriveItem[], folderPath : string, recursive = false) : Promise<GDriveItem[]>
    {
        const missingFiles : GDriveItem[] = [];
        const contents = this.splitContents(items);

        // Check the files
        for(const file of contents.files)
        {
            const filename = file.name.replaceAll('/', '-');
            const filePath = `${ folderPath }/${ filename }`;
            if(!await filesRA.exists(filePath))
            {
                missingFiles.push(file);
            }
        }

        // Handle recursive
        if(recursive)
        {
            const folders = contents.folders;
            for(const folder of folders)
            {
                const folderContents = items.filter((item) => item.parents.includes(folder.id));
                missingFiles.push(...await this.findMissingFiles(folderContents, `${ folderPath }/${ folder.name }`, recursive));
            }
        }

        return missingFiles;
    }

    /**
     * Compare the contents of a folder with known files. Returns a filtered list of new or updated files.
     *
     * @param contents - List of GDriveItems, should be the contents of the folder.
     * @param knownFiles - List of GDriveItems, should be the known files in the folder.
     * @param recursive - Whether to compare files in subfolders.
     *
     * @returns A list of new or updated files.
     */
    diffFolderContents(contents : GDriveItem[], knownFiles : GDriveItem[], recursive = false) : GDriveItem[]
    {
        const knownFileMap = new Map(knownFiles.map((file) => [ file.id, file ]));
        const newFiles : GDriveItem[] = [];

        for(const file of contents)
        {
            const knownFile = knownFileMap.get(file.id);
            const isDir = file.mimeType === 'application/vnd.google-apps.folder';
            if(!knownFile || (!isDir && knownFile.md5Checksum !== file.md5Checksum))
            {
                newFiles.push(file);
            }
        }

        if(recursive)
        {
            const folders = contents.filter((item) => item.mimeType === 'application/vnd.google-apps.folder');
            for(const folder of folders)
            {
                const folderContents = contents.filter((item) => item.parents.includes(folder.id));
                const knownFolderFiles = knownFiles.filter((item) => item.parents.includes(folder.id));
                newFiles.push(...this.diffFolderContents(folderContents, knownFolderFiles, recursive));
            }
        }

        return newFiles;
    }

    /**
     * Get the files to download from a folder.
     *
     * @param folderID - ID of the folder to get files from.
     * @param folderPath - Path to the folder on the filesystem.
     * @param recursive - Whether to get files from subfolders.
     *
     * @returns A list of GDriveItems that are files.
     */
    async getFilesToDownload(folderID : string, folderPath ?: string, recursive = false) : Promise<GDriveItem[]>
    {
        let filesToDownload : GDriveItem[] = [];

        // Get folder contents
        const driveItems = await driveRA.listFolder(folderID, recursive);

        // Get known files from DB
        const dbItems = await driveRA.listFolderDB(folderID, recursive);

        // Check if the files exist on the filesystem
        if(folderPath)
        {
            filesToDownload = [ ...await this.findMissingFiles(driveItems, folderPath, recursive) ];
        }

        // Compare contents with known files
        filesToDownload = [ ... filesToDownload, ...this.diffFolderContents(driveItems, dbItems, recursive) ];

        // Return all the files to Download
        return filesToDownload;
    }

    /**
     * Update the database with the contents of a folder.
     *
     * @param contents - List of GDriveItems to update.
     *
     * @returns A promise that resolves when the update is complete.
     */
    async syncToDB(contents : GDriveItem[]) : Promise<void>
    {
        for(const item of contents)
        {
            await driveRA.addDBItem(item);
        }
    }
}

// ---------------------------------------------------------------------------------------------------------------------
