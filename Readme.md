# Nero Cover Downloader

This is a simple script to download and tag the Neuro-sama Karaoke covers from the "official" Google Drive folder:

* https://drive.google.com/drive/folders/118gr4QuaGQGKfJ0X8VBCytvPjdzPayPY

So main reason for this is that I'm anal-retentive about tagging my music and I love the Neuro covers, but my previous 
approach ([kara][]) was a huge pain in the ass to do manually, and downloading from youtube _sucks_.

## Set Up

In order for this to work, you need a `service-account.json` which is your service account. Google how to get this.

### AcoustID

You will need to install `fpcalc` from [chromaprint][].

**OSX using Homebrew**

```
$ brew install chromaprint
```

**Ubuntu**

```
$ sudo apt-get install libchromaprint-tools
```

**Windows**

```
$ echo "BAHAHAHAHAHAHAHAHAHAHAHAH. Good one. ;)"
```

Also, you'll need a https://acoustid.org/ api key. This goes in a file named `acoustid.json`, and should look like:

```json
{
    "key": "..."
}
```

(This is a free signup.)

<!-- Links -->

[chromaprint]: https://acoustid.org/chromaprint
[kara]: https://gitlab.com/morgul/kara
