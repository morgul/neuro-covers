// ----------------------------------------------------------------------------------------------------------------------r
// Knex Migration configuration
//----------------------------------------------------------------------------------------------------------------------

import type { Knex } from 'knex';

// Managers
import { getDB, getDBConfig } from './src/utils/database.js';

//----------------------------------------------------------------------------------------------------------------------

module.exports = async() =>
{
    const db = await getDB();

    // When this file is run, it expects the migrations to end in .ts, and we were changing the migrations to end in
    // .js. So, now we need to change them back.
    await db('knex_migrations')
        .select()
        .limit(1)
        .then(async() =>
        {
            await db.update({ name: db.raw('replace(name, \'.js\', \'.ts\')') })
                .from('knex_migrations');
        })
        .catch(async(error) =>
        {
            if(error.code !== 'SQLITE_ERROR')
            {
                throw error;
            } // end if
        });

    const config = await getDBConfig();

    return {
        ...config ?? {},
        migrations: {
            directory: './src/knex/migrations'
        },
        seeds: {
            directory: './src/knex/seeds'
        }
    } satisfies Knex.Config;
};

//----------------------------------------------------------------------------------------------------------------------
