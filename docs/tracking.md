# File Tracking

Tracking the right files to download (and keep from pulling all 2+ GB of songs)

## New / Updated Files

In order to track the files that have been updated, or we need to download again, we need to keep track of what's been 
downloaded and what hasn't been, as well as any updates. To do that, the easiest way is for us to record the contents 
of the Google Drive along with the google-calculated `md5Checksum` property. This will allow us to know when there's a 
newer version.

The logic, then, is to list all the files in the Google Drive and compare it's current `md5Checksum` with the one we 
have in the database. If we don't have the file, or the `md5Checksum` is different, we download the file.

## Local Files

For local files, we need to do a similar, but slightly different, process. We can't use the local file's md5 
checksum, because we modify it as part of this. Instead, we just let the existence of the file be the indicator. 
While we could store an updated md5 checksum for the local file and worry about modifications, that's all overkill.

## Logic

1. Get the list of files from the Google Drive
2. Get the list of files from the local directory
3. Compare the two lists
   1. If the file is in the Google Drive but not in the local directory, download it
   2. If the file is in the Google Drive and in the local directory, compare the `md5Checksum` and download if different
   3. If the file is in the local directory but not in the Google Drive, ignore it

